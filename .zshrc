# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
ZSH=/usr/share/oh-my-zsh/

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="spaceship"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git asdf)
# syntax highlighting: zsh-syntax-highlighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
# autosuggestions: zsh-autosuggestions
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
# history search: zsh-history-substring-search
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

ZSH_CACHE_DIR=$HOME/.cache/oh-my-zsh
if [[ ! -d $ZSH_CACHE_DIR ]]; then
  mkdir $ZSH_CACHE_DIR
fi

source $ZSH/oh-my-zsh.sh

# Aliases
alias vpnip="ip addr show dev tun0 | grep inet | cut -d ' ' -f 6"
alias zshconfig="$EDITOR ~/.zshrc"
alias sshconfig="$EDITOR ~/.ssh/config"
alias git-nossl="git -c http.sslVerify=false"
alias sort-ipv4="sort -t . -k 3,3n -k 4,4n"
alias grep-nmap-open="grep -P '\d+\/tcp\s+open' --text"
alias icat="kitty +kitten icat"
alias yt-dlp-song='yt-dlp -f m4a --output "%(title)s.%(ext)s"'
alias yt-dlp-album='yt-dlp -f m4a --output "%(playlist_index)s_%(title)s.%(ext)s"'
alias fzf-wl='find /usr/share/seclists/ /usr/share/wordlists /usr/share/fuzzdb -type f | fzf'
alias fzf-wl-pass='find /usr/share/seclists/Passwords /usr/share/wordlists/passwords -type f | fzf'
alias fzf-wl-web='find /usr/share/seclists/Discovery/Web-Content/ /usr/share/fuzzdb/discovery/predictable-filepaths -type f | fzf'
alias restart-plasma="kquitapp5 plasmashell || killall plasmashell && kstart5 plasmashell"
alias grep-comment-blank="grep -vP '(?:^#)|(?:^$)'"
alias clean-cache-pacman="sudo paccache -rk 1 && paccache -ruk 0"
alias clean-cache-pikaur="sudo paccache -rk 1 --cachedir=/home/noraj/.cache/pikaur/ && paccache -ruk 0 --cachedir=/home/noraj/.cache/pikaur/"

# Color
alias diff='diff --color=auto'
alias grep='grep --color=auto'
alias ls='ls --color=auto'
alias ps='grc ps'
alias df='grc df'
alias mount='grc mount'
alias lsblk='grc lsblk'
export LESS=-R
man() {
  LESS_TERMCAP_md=$'\e[01;31m' \
  LESS_TERMCAP_me=$'\e[0m' \
  LESS_TERMCAP_se=$'\e[0m' \
  LESS_TERMCAP_so=$'\e[01;44;33m' \
  LESS_TERMCAP_ue=$'\e[0m' \
  LESS_TERMCAP_us=$'\e[01;32m' \
  command man "$@"
}

# default editor
export EDITOR='nvim'
export VISUAL='nvim'
export DIFFPROG="nvim pacdiff"

# https://github.com/spaceship-prompt/spaceship-prompt/discussions/1113
SPACESHIP_VI_MODE_SHOW=false

# https://github.com/open-mpi/hwloc/issues/354
export HWLOC_HIDE_ERRORS=2

# https://github.com/openwall/john/issues/4765
export OMPI_MCA_opal_warn_on_missing_libcuda=0

# TLDR
export TLDR_LANGUAGE="fr"

# Wayland
export ELECTRON_OZONE_PLATFORM_HINT="auto"

# SSH agent
if ! pgrep -u "$USER" ssh-agent > /dev/null; then
    ssh-agent > ~/.ssh-agent-thing
fi
if [[ "$SSH_AGENT_PID" == "" ]]; then
    eval "$(<~/.ssh-agent-thing)"
fi

# TMUX
if which tmux >/dev/null 2>&1; then
    # if no session is started, start a new session
    test -z ${TMUX} && tmux

    # when quitting tmux, try to attach
    while test -z ${TMUX}; do
        tmux attach || break
    done
fi

# GraphicsMagick conflict with oh-my-zsh git aliases
unalias ${(k)aliases[(R)git *]}

# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/noraj/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# AUR spaceship-prompt
source /usr/lib/spaceship-prompt/spaceship.zsh

# private

* `.vimrc2` for neovim, path: `~/.config/nvim/init.vim`
* `.zshrc` (used with [oh-my-zsh-git](https://aur.archlinux.org/packages/oh-my-zsh-git/))
* `.tmux.conf` for tmux
* `vscode_settings.json` for VSCode
* `tlrc.toml` for tlrc, path: `~/.config/tlrc/config.toml`
